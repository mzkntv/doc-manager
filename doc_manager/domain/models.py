import dataclasses
from typing import Set
from uuid import UUID


@dataclasses.dataclass(unsafe_hash=True)
class Document:
    id: UUID = dataclasses.field(hash=True)
    version: int = 1


@dataclasses.dataclass(unsafe_hash=True)
class Product:
    id: UUID = dataclasses.field(hash=True)
    name: str
    documents: Set[Document] = dataclasses.field(default_factory=set)
