import abc
from typing import List
from uuid import UUID

from sqlalchemy.orm import Session

from ..domain.models import Product


class AbstractProductRepository(abc.ABC):
    """
    Abstract product repository interface
    """

    @abc.abstractmethod
    def get(self, product_id: UUID) -> Product:
        raise NotImplementedError

    @abc.abstractmethod
    def add(self, product: Product) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def list(self) -> List[Product]:
        raise NotImplementedError


class SqlAlchemyProductRepository(AbstractProductRepository):
    def __init__(self, session: Session) -> None:
        self.session = session

    def get(self, product_id: UUID) -> Product:
        return self.session.query(Product).where(id=product_id)

    def add(self, product: Product) -> None:
        self.session.add(product)

    def list(self) -> List[Product]:
        return self.session.query(Product).all()
