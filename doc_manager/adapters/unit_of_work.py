from __future__ import annotations
import abc
from typing import Callable

from sqlalchemy.orm import Session

from . import repositories


class AbstractUnitOfWork(abc.ABC):
    products: repositories.AbstractProductRepository

    def __enter__(self) -> AbstractUnitOfWork:
        return self

    def __exit__(self, *args, **kwargs):
        self.rollback()

    @abc.abstractmethod
    def commit(self) -> None:
        """
        Should be called when business process was completed to synchronize changes with persistent storage
        :return: None
        """
        raise NotImplementedError

    @abc.abstractmethod
    def rollback(self) -> None:
        """
        Called if something went wrong in business process to prevent synchronizing with persistent storage
        :return: None
        """
        raise NotImplementedError


class SqlAlchemyUnitOfWork(AbstractUnitOfWork):
    def __init__(self, session_factory: Callable[..., Session]):
        self.session_factory = session_factory

    def __enter__(self):
        self.session = self.session_factory()
        self.products = repositories.SqlAlchemyProductRepository(self.session)

    def __exit__(self, *args, **kwargs):
        super(SqlAlchemyUnitOfWork, self).__exit__()
        self.session.close()

    def commit(self) -> None:
        self.session.commit()

    def rollback(self) -> None:
        self.session.rollback()
